//
//  ViewController.swift
//  Accommodation
//
//  Created by 안현선 on 2016. 2. 24..
//  Copyright © 2016년 안현선. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: Properties
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var AddressLabel: UILabel!
    @IBOutlet weak var PhoneLabel: UILabel!
    @IBOutlet weak var HomepageLabel: UILabel!
    
    /*
    This value is either passed by `MealTableViewController` in `prepareForSegue(_:sender:)`
    or constructed as part of adding a new meal.
    */
    var accommodation: Accommodation?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up views if editing an existing Meal.
        if let accommodation = accommodation {
            NameLabel.text   = accommodation.name
            AddressLabel.text = accommodation.address
            PhoneLabel.text = accommodation.phone
            HomepageLabel.text = accommodation.homepage
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

