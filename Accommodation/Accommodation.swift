//
//  Accommodation.swift
//  Accommodation
//
//  Created by 안현선 on 2016. 2. 25..
//  Copyright © 2016년 안현선. All rights reserved.
//

import UIKit
import ObjectMapper

class Accommodation: Mappable {
    // MARK: Properties
    
    var name: String?
    var address: String?
    var phone: String?
    var homepage: String?
    
    // MARK: Initialization
    
    init?(address: String, homepage: String, phone: String,name: String) {
        // Initialize stored properties.
        self.name = name
        self.address = address
        self.phone = phone
        self.homepage = homepage
        
        // Initialization should fail if there is no name or if the rating is negative.
        if name.isEmpty || address.isEmpty || phone.isEmpty || homepage.isEmpty {
            return nil
        }
    }

    required init?(_ map: Map) {
        mapping(map)
    }
    
    func mapping(map: Map) {
        self.address <- map["address.value"]
        self.homepage <- map["homepage.value"]
        self.phone <- map["phone.value"]
        self.name <- map["name.value"]
    }
    
}
