//
//  AccommodationTableViewController.swift
//  Accommodation
//
//  Created by 안현선 on 2016. 2. 25..
//  Copyright © 2016년 안현선. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class AccommodationTableViewController: UITableViewController {
    // MARK: Properties
    @IBOutlet var accommodationTableView: UITableView!
    
    var accommodations = [Accommodation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let hostString = "http://localhost:3000/home/index.json"
        let url: NSURL = NSURL(string: hostString)!
        Alamofire.request(.GET, url)
            .responseJSON { response in
            switch response.result {
            case .Success(let data):
                let jsonArray = response.result.value
                
                self.accommodations = Mapper<Accommodation>().mapArray(jsonArray)!
                
                //self.accommodationTableView.reloadData()
                self.tableView.reloadData()
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count: Int!
        if let list: [Accommodation]? = self.accommodations {
            count = list?.count
        }
        else {
            count = 0
        }
        return count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "AccommodationTableViewCell"
        let cell = self.accommodationTableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! AccommodationTableViewCell
        
        // Fetches the appropriate meal for the data source layout.
        
        if let accommodation: Accommodation = self.accommodations[indexPath.row] as Accommodation? {
            cell.nameLabel.text = accommodation.name
            cell.addressLabel.text = accommodation.address
            cell.phoneLabel.text = accommodation.phone
            cell.homepageLabel.text = accommodation.homepage
            print(cell.homepageLabel.text)
        }
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowDetail" {
            let accommodationDetailViewController = segue.destinationViewController as! ViewController
            // Get the cell that generated this segue.
            if let selectedAccommodationCell = sender as? AccommodationTableViewCell {
                let indexPath = tableView.indexPathForCell(selectedAccommodationCell)!
                let selectedAccommodation = accommodations[indexPath.row]
                accommodationDetailViewController.accommodation = selectedAccommodation
            }
        }
    }


}
