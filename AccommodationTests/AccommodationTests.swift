//
//  AccommodationTests.swift
//  AccommodationTests
//
//  Created by 안현선 on 2016. 2. 24..
//  Copyright © 2016년 안현선. All rights reserved.
//

import XCTest
@testable import Accommodation

class AccommodationTests: XCTestCase {
    
    // MARK: FoodTracker Tests
    
    // Tests to confirm that the Meal initializer returns when no name or a negative rating is provided.
    func testMealInitialization() {
        // Success case.
        let potentialItem = Accommodation(name: "Newest meal", address: "Seoul", phone: "010-1234-1234", homepage: "www.naver.com")
        XCTAssertNotNil(potentialItem)
        
        // Failure cases.
        let noName = Accommodation(name: "", address: "", phone: "", homepage: "")
        XCTAssertNil(noName, "Empty name is invalid")
        
        let badRating = Accommodation(name: "Really bad rating", address: "Busan", phone: "1234", homepage: "www.daum.com")
        XCTAssertNotNil(badRating)
    }
}
